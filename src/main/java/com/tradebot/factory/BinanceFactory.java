package com.tradebot.factory;

import java.math.BigDecimal;
import java.util.List;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.TimeInForce;
import com.binance.api.client.domain.account.NewOrder;
import com.binance.api.client.domain.account.NewOrderResponse;
import com.binance.api.client.domain.account.Order;
import com.binance.api.client.domain.account.request.CancelOrderRequest;
import com.binance.api.client.domain.account.request.OrderStatusRequest;
import com.binance.api.client.domain.market.TickerStatistics;

public class BinanceFactory {
	private static final String APIKEY = "qPcunZZ7K4B7TpjgkQ6r1ukxOfBIvGE0Wow6iyeLQdDyMspAtGfoQ1U4nvcdyWOi";
	private static final String APISECRET = "ygYldnqrpWVZhAM11TYCkrT3KxPdt93TRfnk71Vo7c2cWHZwT7wafWbMOw6zUN7u";
	
	private static volatile BinanceFactory factory;
	private static volatile BinanceApiRestClient client;
	private static volatile BigDecimal quantidadeBtcDisponivel;
	private static volatile Integer quantidadeBots;
	
	private BinanceFactory() {};
	
	public void setQuantidadeBots(Integer quantidade) {
		quantidadeBots = quantidade;
		BigDecimal qtdBTCLivre = new BigDecimal(client.getAccount().getAssetBalance("BTC").getFree());
		
		if(quantidade != null && quantidade > 0) {
			quantidadeBtcDisponivel = qtdBTCLivre.divide(new BigDecimal(quantidadeBots));
		} else {
			quantidadeBtcDisponivel = qtdBTCLivre;
		}
	}
	
	public static BinanceFactory instance(String apiKey, String apiSecret) {
		if(client == null) {
			synchronized (BinanceFactory.class) {
				if (client == null) {
					BinanceApiClientFactory syncFactory = BinanceApiClientFactory.newInstance(apiKey, apiSecret);
					client = syncFactory.newRestClient();
					factory = new BinanceFactory();
				}
			}
		}
		
		return factory;
	}
	
	public static BinanceFactory instance() {
		return factory;
	}
	
	public NewOrderResponse comprar(String moeda, BigDecimal valor) {
		synchronized (BinanceFactory.class) {
			BigDecimal quantidade = quantidadeBtcDisponivel.divide(valor, 0, BigDecimal.ROUND_DOWN);
			return client.newOrder(NewOrder.limitBuy(moeda, TimeInForce.GTC, quantidade.toString(), valor.toString()));
		}
	}
	
	public NewOrderResponse vender(String moeda, BigDecimal valor, BigDecimal quantidade) {
		return client.newOrder(NewOrder.limitSell(moeda, TimeInForce.GTC, quantidade.toString(), valor.toString()));
	}
	
	public Order statusOrdem(String moeda, Long orderId) {
		return client.getOrderStatus(new OrderStatusRequest(moeda, orderId));
	}
	
	public void cancelarOrdem(String moeda, Long orderId) {
		client.cancelOrder(new CancelOrderRequest(moeda, orderId));
	}
	
	public BigDecimal obterValorMoeda(String moeda) {
		return new BigDecimal(client.getPrice(moeda).getPrice());
	}
	
	public List<TickerStatistics> getEstatistica24HorasTodasMoedas(){
		return client.getAll24HrPriceStatistics();
	}
	
	public static void main(String[] args) {
		String apiKey = "9oG7xDQAZZg67LP5RJ0THyg422XYjOaCVcng87fJv2qWxjRbg7s2WLYNeunaSp8x";
		String apiSecret = "LoHtxWpLyIqkePblwPR4p1mC0LOwJIEp6V0jGQ7BM3mKc3zvOiTpWQz2TVonVhHq";
		
//		BinanceFactory.instance(apiKey, apiSecret);
//		BinanceFactory.instance().setQuantidadeBots(1);
//		BinanceFactory.instance().comprar("BCNBTC", new BigDecimal("0.00000121"));
		
		
	}
	
}
