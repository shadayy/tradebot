package com.tradebot.entity;

import java.math.BigDecimal;

public class Parametros {
	private String moeda;
	private BigDecimal compra;
	private BigDecimal venda;
	private BigDecimal taxa;
	private String nomeRobo;
	private Long tempoEspera;
	
	public String getMoeda() {
		return moeda;
	}
	
	public void setMoeda(String moeda) {
		this.moeda = moeda;
	}
	
	public BigDecimal getCompra() {
		return compra;
	}
	
	public void setCompra(BigDecimal compra) {
		this.compra = compra;
	}
	
	public BigDecimal getVenda() {
		return venda;
	}
	
	public void setVenda(BigDecimal venda) {
		this.venda = venda;
	}

	public BigDecimal getTaxa() {
		return taxa;
	}

	public void setTaxa(BigDecimal taxa) {
		this.taxa = taxa;
	}
	
	
	public String getNomeRobo() {
		if(nomeRobo != null) {
			return nomeRobo;
		} else {
//			return this.moeda.concat(" ").concat(this.compra.toString()).concat(" - ").concat(this.venda.toString());
		}
		return this.moeda;
	}

	public void setNomeRobo(String nomeRobo) {
		this.nomeRobo = nomeRobo;
	}
	
	public Long getTempoEspera() {
		return tempoEspera;
	}

	public void setTempoEspera(Long tempoEspera) {
		this.tempoEspera = tempoEspera;
	}

	public void normalizarPreco(BigDecimal valor) {
		if(this.compra.compareTo(valor) > 0) {
			//this.compra = valor.multiply( (BigDecimal.ONE.subtract(this.taxa.multiply(new BigDecimal("2")))) ).setScale(8, BigDecimal.ROUND_DOWN);
			//this.venda = this.compra.multiply((BigDecimal.ONE.add(this.taxa.multiply(new BigDecimal("2"))))).setScale(8, BigDecimal.ROUND_UP);
			
			this.compra = valor.subtract(new BigDecimal("0.00000001"));
			this.venda = valor;
			
			if(this.venda.compareTo(this.compra) <= 0) {
				aumentarValorVenda(3);
			}
			
			System.out.println("Parametro atualizado: " + this);
		}
	}
	
	private void aumentarValorVenda(Integer qtdTaxa) {
		BigDecimal novaVenda = this.compra.multiply((BigDecimal.ONE.add(this.taxa.multiply(new BigDecimal(qtdTaxa))))).setScale(8, BigDecimal.ROUND_UP);
		if (novaVenda.compareTo(this.venda) <= 0) {
			aumentarValorVenda((qtdTaxa = qtdTaxa + 1));
		}
		
		if(qtdTaxa > 10) {
			throw new RuntimeException("Impossivel normalizar valores: "+ this);
		}
	}

	@Override
	public String toString() {
		return "Parametros [moeda=" + moeda + ", compra=" + compra + ", venda=" + venda + ", taxa=" + taxa
				+ ", nomeRobo=" + nomeRobo + ", esperaEntreTrade=" + tempoEspera + "]";
	}
	
}