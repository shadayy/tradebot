package com.tradebot.entity;

import java.math.BigDecimal;

public class ParametrosAnalise {
	private String baseMoeda;
	private BigDecimal volume;
	private BigDecimal compra;
	private BigDecimal venda;
	private BigDecimal abertura;
	private BigDecimal encerramento;
	private BigDecimal percentualAtual;
	private Integer nivelMedia;
	
	public String getBaseMoeda() {
		return baseMoeda;
	}
	public void setBaseMoeda(String baseMoeda) {
		this.baseMoeda = baseMoeda;
	}
	public BigDecimal getVolume() {
		return volume;
	}
	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}
	public BigDecimal getCompra() {
		return compra;
	}
	public void setCompra(BigDecimal compra) {
		this.compra = compra;
	}
	public BigDecimal getVenda() {
		return venda;
	}
	public void setVenda(BigDecimal venda) {
		this.venda = venda;
	}
	public BigDecimal getAbertura() {
		return abertura;
	}
	public void setAbertura(BigDecimal abertura) {
		this.abertura = abertura;
	}
	public BigDecimal getEncerramento() {
		return encerramento;
	}
	public void setEncerramento(BigDecimal encerramento) {
		this.encerramento = encerramento;
	}
	public Integer getNivelMedia() {
		return nivelMedia;
	}
	public void setNivelMedia(Integer nivelMedia) {
		this.nivelMedia = nivelMedia;
	}
	public BigDecimal getPercentualAtual() {
		return percentualAtual;
	}
	public void setPercentualAtual(BigDecimal percentualAtual) {
		this.percentualAtual = percentualAtual;
	}
	
	
}
