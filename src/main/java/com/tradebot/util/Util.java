package com.tradebot.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class Util {
	
	private Util() {
	}
	
	public static <T> T lerParametro(String fonteDados, Class<T> clazz) {
		String stringFromSource = getStringFromSource(fonteDados);
		Type type = new TypeToken<T>(){}.getType();
		return new Gson().fromJson(stringFromSource, type);
	}
	
	public static <T> List<T> lerParametros(String fonteDados, Class<T> clazz) {
		String stringFromSource = getStringFromSource(fonteDados);
		Type listType = new TypeToken<ArrayList<T>>(){}.getType();
		return new Gson().fromJson(stringFromSource, listType);
	}
	
	private static String getStringFromSource(String fonteDados) {
		BufferedReader in = null;
		FileReader fileReader = null;
		
		try {
			if(new File(fonteDados).exists()) {
				fileReader = new FileReader(new File(fonteDados));
				in = new BufferedReader(fileReader);
			} else {
				int qpos = fonteDados.indexOf('?');
				int hpos = fonteDados.indexOf('#');
				char sep = qpos == -1 ? '?' : '&';
				String seg = sep + "_"+ System.currentTimeMillis() + '=' + System.currentTimeMillis();
				fonteDados = hpos == -1 ? fonteDados + seg : fonteDados.substring(0, hpos) + seg + fonteDados.substring(hpos);
				
				URL url = new URL(fonteDados);
				URLConnection urlConn = url.openConnection();
				urlConn.setUseCaches(false);
				
				in = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
			}
			
			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
			
			in.close();
			return sb.toString();
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			if(fileReader != null) {
				try {
					fileReader.close();
				} catch (IOException e) {
				}
			}
			
			if(in != null) {
				try {
					in.close();
				} catch (IOException e) {
				}
			}
		}
	}
	
}
