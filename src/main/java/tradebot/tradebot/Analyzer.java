package tradebot.tradebot;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import com.binance.api.client.domain.market.TickerStatistics;
import com.tradebot.entity.ParametrosAnalise;
import com.tradebot.factory.BinanceFactory;

public class Analyzer {
	
	public static void main(String[] args) {
		String APIKEY = "qPcunZZ7K4B7TpjgkQ6r1ukxOfBIvGE0Wow6iyeLQdDyMspAtGfoQ1U4nvcdyWOi";
		String APISECRET = "ygYldnqrpWVZhAM11TYCkrT3KxPdt93TRfnk71Vo7c2cWHZwT7wafWbMOw6zUN7u";
		
		process(APIKEY, APISECRET, null);
	}
	
	private static void process(String apiKey, String apiSecret, String fonteDados) {
		try {
//			ParametrosAnalise parametro = Util.lerParametro(fonteDados, ParametrosAnalise.class);
			ParametrosAnalise parametro = new ParametrosAnalise();
			parametro.setBaseMoeda("BTC");
			parametro.setVolume(new BigDecimal("4000"));
			parametro.setNivelMedia(-1);
			parametro.setPercentualAtual(BigDecimal.ZERO);
			
			List<TickerStatistics> moedas = BinanceFactory.instance(apiKey, apiSecret).getEstatistica24HorasTodasMoedas();
			
			if(parametro.getBaseMoeda() != null) {
				moedas = moedas.stream().filter(moeda ->{
					String baseMoeda = moeda.getSymbol().substring(parametro.getBaseMoeda().length(), moeda.getSymbol().length());
					return baseMoeda.equals(parametro.getBaseMoeda());
				}).collect(Collectors.toList());
			}
			
			if(parametro.getVolume() != null) {
				moedas = moedas.stream().filter(moeda ->{
					BigDecimal volumeBaseMoeda = new BigDecimal(moeda.getVolume()).multiply(new BigDecimal(moeda.getAskPrice()));
					return volumeBaseMoeda.compareTo(parametro.getVolume()) >= 0;
				}).collect(Collectors.toList());
			}
			
			if(parametro.getCompra() != null) {
				moedas = moedas.stream().filter(moeda ->{
					return new BigDecimal(moeda.getBidPrice()).compareTo(parametro.getCompra()) >= 0;
				}).collect(Collectors.toList());
			}
			
			if(parametro.getVenda() != null) {
				moedas = moedas.stream().filter(moeda ->{
					return new BigDecimal(moeda.getAskPrice()).compareTo(parametro.getVenda()) >= 0;
				}).collect(Collectors.toList());
			}
			
			if(parametro.getNivelMedia() != null) {
				moedas = moedas.stream().filter(moeda ->{
					return new BigDecimal(moeda.getBidPrice()).compareTo(new BigDecimal(moeda.getWeightedAvgPrice())) <= parametro.getNivelMedia();
				}).collect(Collectors.toList());
			}
			
			if(parametro.getPercentualAtual() != null) {
				moedas = moedas.stream().filter(moeda ->{
					System.out.println(parametro.getPercentualAtual());
					System.out.println(new BigDecimal(moeda.getPriceChangePercent()));
					System.out.println(new BigDecimal(moeda.getPriceChangePercent()).compareTo(parametro.getPercentualAtual()));
					System.out.println("----------");
					return new BigDecimal(moeda.getPriceChangePercent()).compareTo(parametro.getPercentualAtual()) <= 0;
				}).collect(Collectors.toList());
			}
			
			if(parametro.getAbertura() != null) {
				moedas = moedas.stream().filter(moeda ->{
					return new BigDecimal(moeda.getOpenPrice()).compareTo(parametro.getAbertura()) >= 0;
				}).collect(Collectors.toList());
			}
			
			if(parametro.getEncerramento() != null) {
				moedas = moedas.stream().filter(moeda ->{
					return new BigDecimal(moeda.getBidPrice()).compareTo(parametro.getEncerramento()) >= 0;
				}).collect(Collectors.toList());
			}
			
			System.out.println(moedas);
		} catch (Exception e) {
			
		}
	}
	
	private static void getPrecoReal(String moeda) {
		
	}
}
