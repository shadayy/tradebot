package tradebot.tradebot;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.tradebot.entity.Parametros;
import com.tradebot.factory.BinanceFactory;
import com.tradebot.util.Util;

public class App {
	public static final Long TEMPO_SLEEP_EXCEPTION = 1000L;
	public static final Long TEMPO_SLEEP_CONSULTA_API= 250L;
	public static final Long TEMPO_SLEEP_LEITURA_PARAMETROS= 120000L * 5L; //2 minutos
	
	private static Map<String, Object[]> workers = new HashMap<String, Object[]>();
	
	public static void main(String[] args) {
		args = new String[3];
		
		args[0] = "qPcunZZ7K4B7TpjgkQ6r1ukxOfBIvGE0Wow6iyeLQdDyMspAtGfoQ1U4nvcdyWOi";
		args[1] = "ygYldnqrpWVZhAM11TYCkrT3KxPdt93TRfnk71Vo7c2cWHZwT7wafWbMOw6zUN7u";
		args[2] = "http://tradebot.myartsonline.com/salvarTrade.php?acao=listar";
		
		if(args.length < 3) {
			Parametros parametros = new Parametros();
			parametros.setMoeda("TRXBTC");
			parametros.setCompra(new BigDecimal("0.00000521"));
			parametros.setVenda(new BigDecimal("0.00000528"));
			parametros.setTaxa(new BigDecimal("0.01"));
			parametros.setTempoEspera(1000l);
			
			System.out.println("Instrucoes de uso:");
			System.out.println("Parametro 1- APIKEY");
			System.out.println("Parametro 2- APISECRET");
			System.out.println("Parametro 3- Fonte de dados (arquivo ou url)");
			System.out.println("Exemplo de fonte de dados:");
			System.out.println(new Gson().toJson(Arrays.asList(parametros)));
			return;
		}
		
		System.out.println("Parametro 1- APIKEY "+ args[0]);
		System.out.println("Parametro 2- APISECRET "+ args[1]);
		System.out.println("Parametro 3- Fonte de dados: "+ args[2]);
		
		process(args[0], args[1], args[2]);
	}
	
	private static void process(String apiKey, String apiSecret, String fonteDados) {
		while(true) {
			try {
				List<Parametros> parametros = Util.lerParametros(fonteDados, Parametros.class);
				System.out.println(parametros);
				BinanceFactory.instance(apiKey, apiSecret).setQuantidadeBots(parametros.size());
				
				if(parametros.isEmpty()) {
					Iterator<Entry<String, Object[]>> iteratorWorker = workers.entrySet().iterator();
					while (iteratorWorker.hasNext()) {
						Entry<String, Object[]> entry = iteratorWorker.next();
						((Worker) entry.getValue()[0]).encerrar();
						workers.remove(entry.getKey());
					}
					
					process(apiKey, apiSecret, fonteDados);
				}
				
				if(!workers.isEmpty()) {
					Iterator<Entry<String, Object[]>> iteratorWorker = workers.entrySet().iterator();
					
					loopWorker:
					while (iteratorWorker.hasNext()) {
						Entry<String, Object[]> entry = iteratorWorker.next();
						
						Iterator<Parametros> iteratorParametro = parametros.iterator();
						while (iteratorParametro.hasNext()) {
							Parametros parametro = iteratorParametro.next();
							
							if(entry.getKey().equals(parametro.getNomeRobo())) {
								Thread thread = (Thread) entry.getValue()[1];
								
								if(thread.isAlive()) {
									((Worker) entry.getValue()[0]).setParametro(parametro);
								} else {
									Worker worker = new Worker(parametro);
									thread = new Thread(worker);
									thread.start();
									entry.setValue(new Object[]{worker, thread});
								}
								
								parametros.remove(parametro);
								continue loopWorker;
							}
						}
						
						((Worker) entry.getValue()[0]).encerrar();
						workers.remove(entry.getKey());
					}
				}
				
				for (Parametros parametro : parametros) {
					Worker worker = new Worker(parametro);
					Thread thread = new Thread(worker);
					thread.start();
					workers.put(parametro.getNomeRobo(), new Object[]{worker, thread});
				}
				
				Thread.sleep(App.TEMPO_SLEEP_LEITURA_PARAMETROS);
				process(apiKey, apiSecret, fonteDados);
			} catch (Exception e) {
				e.printStackTrace();
				process(apiKey, apiSecret, fonteDados);
			}
		}
	}
}
