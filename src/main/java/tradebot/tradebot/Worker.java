package tradebot.tradebot;

import java.math.BigDecimal;

import com.binance.api.client.domain.OrderStatus;
import com.binance.api.client.domain.account.NewOrderResponse;
import com.binance.api.client.domain.account.Order;
import com.tradebot.entity.Parametros;
import com.tradebot.factory.BinanceFactory;

public class Worker implements Runnable {
	private volatile Parametros parametro;
	private volatile Order statusCompra;
	private volatile boolean running;
	
	public Worker(Parametros parametros) {
		parametro = parametros;
	}
	
	public Parametros getParametro() {
		return parametro;
	}

	public void setParametro(Parametros parametroAtualizar) {
		if(statusCompra != null && !(statusCompra.getStatus().equals(OrderStatus.FILLED) && !(statusCompra.getStatus().equals(OrderStatus.CANCELED)))) {
			if( !(parametro.getMoeda().equals(parametroAtualizar.getMoeda()))
				|| !(parametro.getCompra().equals(parametroAtualizar.getCompra()))
				|| !(parametro.getVenda().equals(parametroAtualizar.getVenda()))
				|| !(parametro.getTaxa().equals(parametroAtualizar.getTaxa()))
			) {
				System.out.println("Cancelando ordem para alteracao de parametros do robo "+ parametro.getNomeRobo() +" de "+ parametro +" para "+ parametroAtualizar);
				this.parametro = parametroAtualizar;
				BinanceFactory.instance().cancelarOrdem(statusCompra.getSymbol(), statusCompra.getOrderId());
			}
		} else {
			this.parametro = parametroAtualizar;
		}
	}
	
	public void encerrar() {
		System.out.println("Encerrando robo "+ parametro.getNomeRobo());
		if(statusCompra != null && !(statusCompra.getStatus().equals(OrderStatus.FILLED)) && !(statusCompra.getStatus().equals(OrderStatus.CANCELED))) {
			BinanceFactory.instance().cancelarOrdem(statusCompra.getSymbol(), statusCompra.getOrderId());
		}
		
		running = false;
	}

	@Override
	public void run() {
		running = true;
		process();
	}
	
	private void process() {
		while(running) {
			try {
				BigDecimal valorAtualMoeda = BinanceFactory.instance().obterValorMoeda(parametro.getMoeda());
				parametro.normalizarPreco(valorAtualMoeda);
				
				Order statusCompra = comprar(parametro);
				if(statusCompra != null) {
					vender(parametro, statusCompra);
				}
				
				if(parametro.getTempoEspera() != null && parametro.getTempoEspera() > 0) {
					Thread.sleep(parametro.getTempoEspera());
				}
			} catch (Exception e) {
				System.out.println("Exception no robo: "+parametro.getNomeRobo() +" "+ e.getMessage());
				e.printStackTrace();
				
				try {
					Thread.sleep(App.TEMPO_SLEEP_EXCEPTION);
				} catch (InterruptedException e1) {
				}
			}
		}
	}
	
	private Order comprar(Parametros parametro) {
		if(tradeValorNegativo()) {
			throw new RuntimeException("Valor de compra e venda selecionado resultará em PREJUIZO!! "+ parametro);
		}
		
		try {
			NewOrderResponse ordemCompra = BinanceFactory.instance().comprar(parametro.getMoeda(), parametro.getCompra());
			statusCompra = null;
			
			while(true) {
				try {
					Thread.sleep(App.TEMPO_SLEEP_CONSULTA_API);
					statusCompra = BinanceFactory.instance().statusOrdem(parametro.getMoeda(), ordemCompra.getOrderId());
					if(statusCompra.getStatus().equals(OrderStatus.FILLED)) {
						System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@ COMPRA REALIZADA - "+ parametro.getNomeRobo());
						return statusCompra;
					} else if(statusCompra.getStatus().equals(OrderStatus.CANCELED)) {
						return null;
					}
				} catch (Exception e) {
					continue;
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("Exception na compra: "+ parametro.getNomeRobo(), e);
		}
	}
	
	private boolean vender(Parametros parametro, Order statusCompra) {
		try {
			BigDecimal quantidadeVender = new BigDecimal(statusCompra.getExecutedQty()).multiply((BigDecimal.ONE.subtract(parametro.getTaxa())));
			NewOrderResponse ordemVenda = BinanceFactory.instance().vender(parametro.getMoeda(), parametro.getVenda(), quantidadeVender);
			Order statusVenda = null;
			
			while(true) {
				try {
					Thread.sleep(App.TEMPO_SLEEP_CONSULTA_API);
					statusVenda = BinanceFactory.instance().statusOrdem(parametro.getMoeda(), ordemVenda.getOrderId());
					if(statusVenda.getStatus().equals(OrderStatus.FILLED)) {
						System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@ VENDA REALIZADA - "+ parametro.getNomeRobo());
						return true;
					} else if(statusCompra.getStatus().equals(OrderStatus.CANCELED)) {
						return false;
					}
				} catch (Exception e) {
					continue;
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("Exception na venda: "+ parametro.getNomeRobo(), e);
		}
	}
	
	private boolean tradeValorNegativo() {
		BigDecimal quantidade = new BigDecimal("1000");
		
		BigDecimal quantidadeAposCompra = quantidade.multiply(new BigDecimal("1").subtract(parametro.getTaxa()));
		BigDecimal valorSatCompra = quantidade.multiply(parametro.getCompra());
		BigDecimal valorSatVenda = quantidadeAposCompra.multiply(parametro.getVenda()).multiply(new BigDecimal("1").subtract(parametro.getTaxa()));
		
		return valorSatCompra.compareTo(valorSatVenda) > 0;
	}
}
